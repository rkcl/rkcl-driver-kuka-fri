
Overview
=========

TODO: input a short description of package rkcl-driver-kuka-fri utility here



The license that applies to the whole package content is **CeCILL**. Please look at the license.txt file at the root of this repository.

Installation and Usage
=======================

The detailed procedures for installing the rkcl-driver-kuka-fri package and for using its components is based on the [PID](http://pid.lirmm.net/pid-framework/pages/install.html) build and deployment system called PID. Just follow and read the links to understand how to install, use and call its API and/or applications.

For a quick installation:

## Installing the project into an existing PID workspace

To get last version :
 ```
cd <path to pid workspace>/pid
make deploy package=rkcl-driver-kuka-fri
```

To get a specific version of the package :
 ```
cd <path to pid workspace>/pid
make deploy package=rkcl-driver-kuka-fri version=<version number>
```

## Standalone install
 ```
git clone https://gitlab.com/rkcl/rkcl-driver-kuka-fri.git
cd rkcl-driver-kuka-fri
```

Then run the adequate install script depending on your system. For instance on linux:
```
sh share/install/standalone_install.sh
```

The pkg-config tool can be used to get all links and compilation flags for the libraries defined inthe project. To let pkg-config know these libraries, read the last output of the install_script and apply the given command. It consists in setting the PKG_CONFIG_PATH, for instance on linux do:
```
export PKG_CONFIG_PATH=<path to rkcl-driver-kuka-fri>/binaries/pid-workspace/share/pkgconfig:$PKG_CONFIG_PATH
```

Then, to get compilation flags run:

```
pkg-config --static --cflags rkcl-driver-kuka-fri_<name of library>
```

To get linker flags run:

```
pkg-config --static --libs rkcl-driver-kuka-fri_<name of library>
```


About authors
=====================

rkcl-driver-kuka-fri has been developped by following authors: 
+ Sonny Tarbouriech (Tecnalia)
+ Benjamin Navarro (LIRMM)

Please contact Sonny Tarbouriech - Tecnalia for more information or questions.



