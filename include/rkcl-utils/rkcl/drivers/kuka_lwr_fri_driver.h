/*      File: fri_driver.h
 *       This file is part of the program cooperative-task-controller
 *       Program description : Asetofclassesfordual-armcontrolusingthecooperativetaskspacerepresentation
 *       Copyright (C) 2018 -  Sonny Tarbouriech (LIRMM / Tecnalia) Benjamin Navarro (LIRMM). All Right reserved.
 *
 *       This software is free software: you can redistribute it and/or modify
 *       it under the terms of the LGPL license as published by
 *       the Free Software Foundation, either version 3
 *       of the License, or (at your option) any later version.
 *       This software is distributed in the hope that it will be useful,
 *       but WITHOUT ANY WARRANTY without even the implied warranty of
 *       MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *       LGPL License for more details.
 *
 *       You should have received a copy of the GNU Lesser General Public License version 3 and the
 *       General Public License version 3 along with this program.
 *       If not, see <http://www.gnu.org/licenses/>.
 */
/**
 * @file fri_driver.h
 * @date January 9, 2019
 * @author Sonny Tarbouriech
 * @brief Defines a simple FRI driver
 */

#pragma once

#include <rkcl/drivers/driver.h>
#include <vector>
#include <memory>

namespace YAML {
class Node;
}

/**
 * @brief Namespace for everything related to the cooperative task space representation and control
 */
namespace rkcl {

/**
 * @brief Wrapper for the FRI library
 */
class KukaLWRFRIDriver : virtual public Driver {
public:

	KukaLWRFRIDriver(
	    int port,
	    JointGroupPtr joint_group,
	    ObservationPointPtr op_eef);

	KukaLWRFRIDriver(
	    int port,
	    JointGroupPtr joint_group);

	KukaLWRFRIDriver(
	    Robot& robot,
	    const YAML::Node& configuration);

	virtual ~KukaLWRFRIDriver();

	/**
	 * Initialize the communication with V-REP
	 * @param timeout The maximum time to wait to establish the connection.
	 * @return true on success, false otherwise
	 */
	virtual bool init(double timeout = 30.) override;
	/**
	 * @brief Check the state of the connection.
	 * @return True if the connection is still open, false otherwise.
	 */
	bool checkConnection() const;


	/**
	 * @brief Start the simulation.
	 */
	virtual bool start() override;

	/**
	 * @brief Stop the simulation.
	 */
	virtual bool stop() override;

	virtual bool read() override;
	virtual bool send() override;

	virtual bool sync() override;
private:
	static bool registered_in_factory;
	struct pImpl;
	std::unique_ptr<pImpl> impl_;

	ObservationPointPtr op_eef_;
};

using KukaLWRFRIDriverPtr = std::shared_ptr<KukaLWRFRIDriver>;
using KukaLWRFRIDriverConstPtr = std::shared_ptr<const KukaLWRFRIDriver>;
}
