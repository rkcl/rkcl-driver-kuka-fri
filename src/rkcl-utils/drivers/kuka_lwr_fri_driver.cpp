/*      File: vrep_driver.cpp
 *       This file is part of the program cooperative-task-controller
 *       Program description : Asetofclassesfordual-armcontrolusingthecooperativetaskspacerepresentation
 *       Copyright (C) 2018 -  Sonny Tarbouriech (LIRMM / Tecnalia) Benjamin Navarro (LIRMM). All Right reserved.
 *
 *       This software is free software: you can redistribute it and/or modify
 *       it under the terms of the LGPL license as published by
 *       the Free Software Foundation, either version 3
 *       of the License, or (at your option) any later version.
 *       This software is distributed in the hope that it will be useful,
 *       but WITHOUT ANY WARRANTY without even the implied warranty of
 *       MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *       LGPL License for more details.
 *
 *       You should have received a copy of the GNU Lesser General Public License version 3 and the
 *       General Public License version 3 along with this program.
 *       If not, see <http://www.gnu.org/licenses/>.
 */
#include <rkcl/drivers/kuka_lwr_fri_driver.h>

#include <LWRBaseControllerInterface.h>

#include <yaml-cpp/yaml.h>
#include <iostream>


using namespace rkcl;

bool KukaLWRFRIDriver::registered_in_factory = DriverFactory::add<KukaLWRFRIDriver>("fri");

struct KukaLWRFRIDriver::pImpl {
	pImpl(double sample_time, int port) :
		sample_time(sample_time)
	{
		fri = std::make_unique<LWRBaseControllerInterface>(sample_time, port);
	}
	std::unique_ptr<LWRBaseControllerInterface> fri;
	double sample_time;
};

KukaLWRFRIDriver::KukaLWRFRIDriver(
    int port,
    JointGroupPtr joint_group,
    ObservationPointPtr op_eef) :
	Driver(joint_group),
	op_eef_(op_eef)
{
	impl_ = std::make_unique<KukaLWRFRIDriver::pImpl>(joint_group_->control_time_step, port);
}

KukaLWRFRIDriver::KukaLWRFRIDriver(
    int port,
    JointGroupPtr joint_group) :
	KukaLWRFRIDriver(port, joint_group, nullptr)
{
}

KukaLWRFRIDriver::KukaLWRFRIDriver(
    Robot& robot,
    const YAML::Node& configuration) :
	op_eef_(nullptr)
{
	std::cout << "Configuring Kuka LWR driver..." << std::endl;
	if(configuration) {
		int port;
		try {
			port = configuration["port"].as<double>();
		}
		catch(...) {
			throw std::runtime_error("KukaLWRFRIDriver::KukaLWRFRIDriver: You must provide a 'port' field in the FRI configuration.");
		}

		std::string joint_group;
		try {
			joint_group = configuration["joint_group"].as<std::string>();
		}
		catch(...) {
			throw std::runtime_error("KukaLWRFRIDriver::KukaLWRFRIDriver: You must provide a 'joint_group' field");
		}
		joint_group_ = robot.getJointGroupByName(joint_group);
		if (not joint_group_)
			throw std::runtime_error("KukaLWRFRIDriver::KukaLWRFRIDriver: unable to retrieve joint group " + joint_group);

		auto op_name = configuration["end-effector_point_name"];
		if(op_name)
		{
			op_eef_ = robot.getObservationPointByName(op_name.as<std::string>());
			if (not op_eef_)
				throw std::runtime_error("KukaLWRFRIDriver::KukaLWRFRIDriver: unable to retrieve end-effector body name");
		}
		impl_ = std::make_unique<KukaLWRFRIDriver::pImpl>(joint_group_->control_time_step, port);
	}
	else {
		throw std::runtime_error("KukaLWRFRIDriver::KukaLWRFRIDriver: The configuration file doesn't include a 'driver' field.");
	}
}

KukaLWRFRIDriver::~KukaLWRFRIDriver() = default;

bool KukaLWRFRIDriver::init(double timeout) {
	std::cout << "\n";
	int result= impl_->fri->StartRobotInJointPositionControl(timeout);

	if(result == EOK) {
		read();
		joint_group_->command.position = joint_group_->state.position;
	}
	else {
		throw std::runtime_error("KukaLWRFRIDriver::init: ERROR, could not start '" + joint_group_->name + "' Kuka LWR");
	}
	return true;
}

bool KukaLWRFRIDriver::checkConnection() const {
	return impl_->fri->IsMachineOK();
}

bool KukaLWRFRIDriver::start() {
	sync();

	std::cout << "\nDualKukaLWRFRIDriver::start: Current system state:" << std::endl << impl_->fri->GetCompleteRobotStateAndInformation() << std::endl;

	return checkConnection();
}

bool KukaLWRFRIDriver::stop() {
	sync();

	bool ok = impl_->fri->StopRobot() == EOK;
	if(not ok) {
		std::cerr << "KukaLWRFRIDriver::stop: An error occurred while stopping the '" << joint_group_->name << "' Kuka LWR" << std::endl;
	}

	return ok;
}

bool KukaLWRFRIDriver::read() {
	float buffer[12];
	bool all_ok = true;

	std::lock_guard<std::mutex> lock(joint_group_->state_mtx);

	if(checkConnection()) {


		if(impl_->fri->GetMeasuredJointPositions(buffer) == EOK) {
			for (size_t i = 0; i < 7; i++) {
				joint_group_->state.position[i] = buffer[i];
			}
		}
		else {
			std::cerr << "KukaLWRFRIDriver::read: Can't get '" << joint_group_->name << "'joint positions from FRI" << std::endl;
			all_ok = false;
		}

		if(impl_->fri->GetEstimatedExternalJointTorques(buffer) == EOK)
		{
			for (size_t i = 0; i < 7; ++i) {
				joint_group_->state.force[i] = buffer[i];
			}
		}
		else {
			std::cerr << "KukaLWRFRIDriver::read: Can't get '" << joint_group_->name << "' external joint torques from FRI" << std::endl;
			all_ok = false;
		}

		if (op_eef_)
		{
			if(impl_->fri->GetEstimatedExternalCartForcesAndTorques(buffer) == EOK)
			{
				// The array comming from FRI is [Fx, Fy, Fz, Tz, Ty, Tx]
				for (size_t i = 0; i < 6; ++i) {
					op_eef_->state.wrench[i] = -buffer[i]; // The provided wrench is the one applied to the environment, not what it is applied to the robot
				}

				// Swapping x & z
				std::swap(op_eef_->state.wrench[3], op_eef_->state.wrench[5]);

			}
			else {
				std::cerr << "KukaLWRFRIDriver::read: Can't get '" << joint_group_->name << "' force sensor values from FRI" << std::endl;
				all_ok = false;
			}
		}
	}
	else {
		std::cerr << "KukaLWRFRIDriver::read: The connection to the '" << joint_group_->name << "' FRI has been lost" << std::endl;
		all_ok = false;
	}

	//Approximation
	joint_group_->state.velocity = joint_group_->command.velocity;

	joint_group_->last_state_update = std::chrono::high_resolution_clock::now();


	return all_ok;
}

bool KukaLWRFRIDriver::send() {
	float buffer[7];

	std::lock_guard<std::mutex> lock(joint_group_->command_mtx);


	if(checkConnection()) {

		joint_group_->command.position = joint_group_->command.position + joint_group_->command.velocity * joint_group_->control_time_step;

		// Set joint target positions
		for (size_t i = 0; i < 7; ++i) {
			buffer[i] = joint_group_->command.position[i];
		}

		impl_->fri->SetCommandedJointPositions(buffer);

		return true;
	}
	else {
		std::cerr << "KukaLWRFRIDriver::send: The connection to the '" << joint_group_->name << "' FRI has been lost" << std::endl;
		return false;
	}
}

bool KukaLWRFRIDriver::sync()  {
	return (impl_->fri->WaitForKRCTick() == EOK);
}
