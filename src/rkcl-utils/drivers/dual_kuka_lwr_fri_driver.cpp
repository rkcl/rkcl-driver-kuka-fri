/*      File: vrep_driver.cpp
 *       This file is part of the program cooperative-task-controller
 *       Program description : Asetofclassesfordual-armcontrolusingthecooperativetaskspacerepresentation
 *       Copyright (C) 2018 -  Sonny Tarbouriech (LIRMM / Tecnalia) Benjamin Navarro (LIRMM). All Right reserved.
 *
 *       This software is free software: you can redistribute it and/or modify
 *       it under the terms of the LGPL license as published by
 *       the Free Software Foundation, either version 3
 *       of the License, or (at your option) any later version.
 *       This software is distributed in the hope that it will be useful,
 *       but WITHOUT ANY WARRANTY without even the implied warranty of
 *       MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *       LGPL License for more details.
 *
 *       You should have received a copy of the GNU Lesser General Public License version 3 and the
 *       General Public License version 3 along with this program.
 *       If not, see <http://www.gnu.org/licenses/>.
 */
#include <rkcl/drivers/dual_kuka_lwr_fri_driver.h>

#include <LWRBaseControllerInterface.h>

#include <yaml-cpp/yaml.h>
#include <iostream>


using namespace rkcl;

bool DualKukaLWRFRIDriver::registered_in_factory = DriverFactory::add<DualKukaLWRFRIDriver>("dual_fri");

struct DualKukaLWRFRIDriver::pImpl {
	pImpl(double sample_time, int port) :
		sample_time(sample_time)
	{
		fri = std::make_unique<LWRBaseControllerInterface>(sample_time, port);
	}
	std::unique_ptr<LWRBaseControllerInterface> fri;
	double sample_time;
};

DualKukaLWRFRIDriver::DualKukaLWRFRIDriver(
    int left_arm_port,
    int right_arm_port,
    JointGroupPtr jg_left_arm,
    JointGroupPtr jg_right_arm,
    ObservationPointPtr op_left_eef,
    ObservationPointPtr op_right_eef) :
	jg_left_arm_(jg_left_arm),
	jg_right_arm_(jg_right_arm),
	op_left_eef_(op_left_eef),
	op_right_eef_(op_right_eef)
{
	left_arm_impl_ = std::make_unique<DualKukaLWRFRIDriver::pImpl>(jg_left_arm_->control_time_step, left_arm_port);
	right_arm_impl_ = std::make_unique<DualKukaLWRFRIDriver::pImpl>(jg_right_arm_->control_time_step, right_arm_port);
}

DualKukaLWRFRIDriver::DualKukaLWRFRIDriver(
    int left_arm_port,
    int right_arm_port,
    JointGroupPtr jg_left_arm,
    JointGroupPtr jg_right_arm) :
	DualKukaLWRFRIDriver(left_arm_port, right_arm_port, jg_left_arm, jg_right_arm, nullptr, nullptr)
{
}

DualKukaLWRFRIDriver::DualKukaLWRFRIDriver(
    Robot& robot,
    const YAML::Node& configuration) :
	op_left_eef_(nullptr),
	op_right_eef_(nullptr)
{
	if(configuration) {
		int left_arm_port, right_arm_port;
		try {
			left_arm_port = configuration["left_arm_port"].as<double>();
		}
		catch(...) {
			throw std::runtime_error("DualKukaLWRFRIDriver::DualKukaLWRFRIDriver: You must provide a 'left_arm_port' field in the FRI configuration.");
		}
		try {
			right_arm_port = configuration["right_arm_port"].as<double>();
		}
		catch(...) {
			throw std::runtime_error("DualKukaLWRFRIDriver::DualKukaLWRFRIDriver: You must provide a 'right_arm_port' field in the FRI configuration.");
		}

		std::string joint_group;
		try {
			joint_group = configuration["left_arm_joint_group"].as<std::string>();
		}
		catch(...) {
			throw std::runtime_error("DualKukaLWRFRIDriver::DualKukaLWRFRIDriver: You must provide a 'left_arm_joint_group' field");
		}
		jg_left_arm_ = robot.getJointGroupByName(joint_group);
		if (not jg_left_arm_)
			throw std::runtime_error("DualKukaLWRFRIDriver::DualKukaLWRFRIDriver: unable to retrieve joint group " + joint_group);

		try {
			joint_group = configuration["right_arm_joint_group"].as<std::string>();
		}
		catch(...) {
			throw std::runtime_error("DualKukaLWRFRIDriver::DualKukaLWRFRIDriver: You must provide a 'right_arm_joint_group' field");
		}
		jg_right_arm_ = robot.getJointGroupByName(joint_group);
		if (not jg_right_arm_)
			throw std::runtime_error("DualKukaLWRFRIDriver::DualKukaLWRFRIDriver: unable to retrieve joint group " + joint_group);

		auto op_name = configuration["left_end-effector_point_name"];
		if(op_name)
		{
			op_left_eef_ = robot.getObservationPointByName(op_name.as<std::string>());
			if (not op_left_eef_)
				throw std::runtime_error("DualKukaLWRFRIDriver::DualKukaLWRFRIDriver: unable to retrieve left_end-effector point name");
		}
		op_name = configuration["right_end-effector_point_name"];
		if(op_name)
		{
			op_right_eef_ = robot.getObservationPointByName(op_name.as<std::string>());
			if (not op_right_eef_)
				throw std::runtime_error("DualKukaLWRFRIDriver::DualKukaLWRFRIDriver: unable to retrieve right_end-effector point name");
		}
		left_arm_impl_ = std::make_unique<DualKukaLWRFRIDriver::pImpl>(jg_left_arm_->control_time_step, left_arm_port);
		right_arm_impl_ = std::make_unique<DualKukaLWRFRIDriver::pImpl>(jg_right_arm_->control_time_step, right_arm_port);
	}
	else {
		throw std::runtime_error("DualKukaLWRFRIDriver::DualKukaLWRFRIDriver: The configuration file doesn't include a 'driver' field.");
	}
}

DualKukaLWRFRIDriver::~DualKukaLWRFRIDriver() = default;

bool DualKukaLWRFRIDriver::init(double timeout) {
	std::cout << "\n";
	int result_left = left_arm_impl_->fri->StartRobotInJointPositionControl(timeout);
	int result_right = right_arm_impl_->fri->StartRobotInJointPositionControl(timeout);
	bool ok = (result_left == EOK && result_right== EOK);

	if(ok) {
		read();
		jg_left_arm_->command.position = jg_left_arm_->state.position;
		jg_right_arm_->command.position = jg_right_arm_->state.position;
	}
	else {
		if (result_left != EOK)
			throw std::runtime_error("DualKukaLWRFRIDriver::init: ERROR, could not start left Kuka LWR");
		else
			throw std::runtime_error("DualKukaLWRFRIDriver::init: ERROR, could not start right Kuka LWR");
	}
	return ok;
}

bool DualKukaLWRFRIDriver::checkConnection() const {
	return left_arm_impl_->fri->IsMachineOK() && right_arm_impl_->fri->IsMachineOK();
}

bool DualKukaLWRFRIDriver::start() {
	sync();

	std::cout << "\nDualKukaLWRFRIDriver::start: Current system state:" << std::endl << left_arm_impl_->fri->GetCompleteRobotStateAndInformation() << std::endl
	          << right_arm_impl_->fri->GetCompleteRobotStateAndInformation() << std::endl;

	return checkConnection();
}

bool DualKukaLWRFRIDriver::stop() {
	sync();

	bool ok_left = left_arm_impl_->fri->StopRobot() == EOK;
	if(not ok_left) {
		std::cerr << "DualKukaLWRFRIDriver::stop: An error occurred while stopping the left Kuka LWR" << std::endl;
	}
	bool ok_right = right_arm_impl_->fri->StopRobot() == EOK;
	if(not ok_right) {
		std::cerr << "DualKukaLWRFRIDriver::stop: An error occurred while stopping the right Kuka LWR" << std::endl;
	}

	return ok_left && ok_right;
}

bool DualKukaLWRFRIDriver::read() {
	float buffer[12];
	bool all_ok = true;

	if(checkConnection()) {

		sync();

		if(left_arm_impl_->fri->GetMeasuredJointPositions(buffer) == EOK) {
			std::lock_guard<std::mutex> lock(jg_left_arm_->state_mtx);
			for (size_t i = 0; i < 7; i++) {
				jg_left_arm_->state.position[i] = buffer[i];
			}
		}
		else {
			std::cerr << "DualKukaLWRFRIDriver::read: Can't get left arm joint positions from FRI" << std::endl;
			all_ok = false;
		}

		if(right_arm_impl_->fri->GetMeasuredJointPositions(buffer) == EOK) {
			std::lock_guard<std::mutex> lock(jg_right_arm_->state_mtx);
			for (size_t i = 0; i < 7; i++) {
				jg_right_arm_->state.position[i] = buffer[i];
			}
		}
		else {
			std::cerr << "DualKukaLWRFRIDriver::read: Can't get right arm joint positions from FRI" << std::endl;
			all_ok = false;
		}

		if(left_arm_impl_->fri->GetEstimatedExternalJointTorques(buffer) == EOK)
		{
			std::lock_guard<std::mutex> lock(jg_left_arm_->state_mtx);
			for (size_t i = 0; i < 7; ++i) {
				jg_left_arm_->state.force[i] = buffer[i];
			}
		}
		else {
			std::cerr << "DualKukaLWRFRIDriver::read: Can't get left arm's external joint torques from FRI" << std::endl;
			all_ok = false;
		}

		if(right_arm_impl_->fri->GetEstimatedExternalJointTorques(buffer) == EOK)
		{
			std::lock_guard<std::mutex> lock(jg_right_arm_->state_mtx);
			for (size_t i = 0; i < 7; ++i) {
				jg_right_arm_->state.force[i] = buffer[i];
			}
		}
		else {
			std::cerr << "DualKukaLWRFRIDriver::read: Can't get right arm's external joint torques from FRI" << std::endl;
			all_ok = false;
		}

		if (op_left_eef_)
		{
			if(left_arm_impl_->fri->GetEstimatedExternalCartForcesAndTorques(buffer) == EOK)
			{
				std::lock_guard<std::mutex> lock(op_left_eef_->state_mtx);
				// The array comming from FRI is [Fx, Fy, Fz, Tz, Ty, Tx]
				for (size_t i = 0; i < 6; ++i) {
					op_left_eef_->state.wrench[i] = -buffer[i]; // The provided wrench is the one applied to the environment, not what it is applied to the robot
				}

				// Swapping x & z
				std::swap(op_left_eef_->state.wrench[3], op_left_eef_->state.wrench[5]);

			}
			else {
				std::cerr << "DualKukaLWRFRIDriver::read: Can't get left arm force sensor values from FRI" << std::endl;
				all_ok = false;
			}
		}
		if (op_right_eef_)
		{
			if(right_arm_impl_->fri->GetEstimatedExternalCartForcesAndTorques(buffer) == EOK)
			{
				std::lock_guard<std::mutex> lock(op_right_eef_->state_mtx);
				// The array comming from FRI is [Fx, Fy, Fz, Tz, Ty, Tx]
				for (size_t i = 0; i < 6; ++i) {
					op_right_eef_->state.wrench[i] = -buffer[i]; // The provided wrench is the one applied to the environment, not what it is applied to the robot
				}

				// Swapping x & z
				std::swap(op_right_eef_->state.wrench[3], op_right_eef_->state.wrench[5]);

			}
			else {
				std::cerr << "DualKukaLWRFRIDriver::read: Can't get right arm force sensor values from FRI" << std::endl;
				all_ok = false;
			}
		}
	}
	else {
		std::cerr << "DualKukaLWRFRIDriver::read: The connection to the FRI has been lost" << std::endl;
		all_ok = false;
	}

	return all_ok;
}

bool DualKukaLWRFRIDriver::send() {
	float buffer[7];

	if(checkConnection()) {

		std::lock_guard<std::mutex> lock_left(jg_left_arm_->command_mtx);
		std::lock_guard<std::mutex> lock_right(jg_right_arm_->command_mtx);

		jg_left_arm_->command.position = jg_left_arm_->command.position + jg_left_arm_->command.velocity * jg_left_arm_->control_time_step;
		jg_right_arm_->command.position = jg_right_arm_->command.position + jg_right_arm_->command.velocity * jg_right_arm_->control_time_step;


		// Set joint target positions
		for (size_t i = 0; i < 7; ++i) {
			buffer[i] = jg_left_arm_->command.position[i];
		}

		left_arm_impl_->fri->SetCommandedJointPositions(buffer);

		// Set joint target positions
		for (size_t i = 0; i < 7; ++i) {
			buffer[i] = jg_right_arm_->command.position[i];
		}

		right_arm_impl_->fri->SetCommandedJointPositions(buffer);

		return true;
	}
	else {
		std::cerr << "DualKukaLWRFRIDriver::send: The connection to the FRI has been lost" << std::endl;
		return false;
	}
}

bool DualKukaLWRFRIDriver::sync()  {
	bool ok = true;
	ok &= (left_arm_impl_->fri->WaitForKRCTick() == EOK);
	ok &= (right_arm_impl_->fri->WaitForKRCTick() == EOK);
	return ok;
}
